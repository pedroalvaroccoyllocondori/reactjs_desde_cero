import React from 'react';
import "../styles/styles.scss";
//import Banner from "../Banner";
import Formulario from "./paginas/Formulario"
import Cursos from './paginas/Cursos';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import Curso from './paginas/Curso';
import MenuPrincipal from './organismos/MenuPrincipal'
import Historial from './paginas/Historial';
import Inicio from './paginas/Inicio';
import Usuarios from './paginas/usuarios';




const App = ()=>(
    <>

    <Router>
      <MenuPrincipal/>
      <Switch>
        <Route path="/" exact component={Inicio}/>
        <Route path="/cursos/:id" exact component={Curso}/>        
        <Route path="/cursos" exact component={Cursos}/>
        <Route path="/historial/:valor" exact component={Historial}/>
        <Route path="/historial" exact component={Historial}/>
        <Route path="/usuarios" exact component={Usuarios}/>
        <Route path="/formulario" exact component={()=><Formulario nombre="pagina de formulario"/>}/>
        <Route component={()=>(
          <div className="ed-grid">
            <h1>error 404 (error de servidor)</h1>
            <span>ruta no encontrada</span>
          </div>
        )}/>
      </Switch>
    </Router>
  </>


)

export default App;
