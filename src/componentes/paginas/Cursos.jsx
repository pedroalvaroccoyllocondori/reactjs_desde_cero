import axios from 'axios'
import React from 'react'
import { Component } from 'react'
import CartaCurso from '../moleculas/CartaCurso'
import GrillaCursos from '../organismos/GrillaCursos'


class Cursos extends Component{

    constructor(props){
        super(props)
        this.state={
            cursos:[]
        }

    }

    componentDidMount(){
        axios.get('https://my-json-server.typicode.com/pedroalvaroccoyllocondori/json-db/cursos')
        .then(respuest=>this.setState({
            cursos:respuest.data
        }))
    }



    render(){

        const{cursos}=this.state
        return <GrillaCursos cursos={cursos} />
    }
}




export default Cursos